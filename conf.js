exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {'browserName': 'chrome'},
    suites: {
        Creation_pos: './Suites/Positive/Creation.js',
        Redaction_pos: './Suites/Positive/Redaction.js',
        Deletion_pos: './Suites/Positive/Deletion.js',
        Positive: ['./Suites/Positive/Creation.js', './Suites/Positive/Redaction.js', './Suites/Positive/Deletion.js'],
        Creation_neg: './Suites/Negative/Creation.js',
        Redaction_neg: './Suites/Negative/Redaction.js',
        Deletion_neg: './Suites/Negative/Deletion.js',
        Negative: ['./Suites/Negative/Creation.js', './Suites/Negative/Deletion.js', './Suites/Negative/Deletion.js']
    },
    onPrepare: function() {
        var AllureReporter = require('jasmine-allure-reporter');
        jasmine.getEnv().addReporter(new AllureReporter({
            resultsDir: 'allure-results'
        }));
    },
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    }
};
