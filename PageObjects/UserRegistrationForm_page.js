var UserRegistrationForm_page = function() {

    var nameField = element(by.id('uname')),
        addressField = element(by.id('address')),
        emailField = element(by.id('email')),
        addUpdateBtn = element(by.id('submit')),
        resetFormBtn = element(by.id('reset'));

    this.nameFieldExist = function() {
        expect(nameField.isPresent()).toBeTruthy();
    };

    this.addUpdateBtnEnabled = function() {
        expect(addUpdateBtn.isEnabled()).toBeTruthy();
    };

    this.addUpdateBtnDisabled = function() {
        expect(addUpdateBtn.isEnabled()).toBeFalsy();
    };

    this.resetFormBtnEnabled = function() {
        expect(resetFormBtn.isEnabled()).toBeTruthy();
    };

    this.resetFormBtnDisabled = function() {
        expect(resetFormBtn.isEnabled()).toBeFalsy();
    };

    this.nameFieldFill = function() {
        nameField.sendKeys('User4');
    };

    this.nameFieldNegativeFill = function() {
        nameField.sendKeys('@#');
    };

    this.nameFieldFill_upd = function() {
        nameField.clear();
        nameField.sendKeys('User5');
    };

    this.nameFieldValueCheck = function() {
        expect(nameField.getAttribute('value')).toEqual('User4');
    };

    this.emailFieldExist = function() {
        expect(emailField.isPresent()).toBeTruthy();
    };

    this.emailFieldFillPositive = function() {
        emailField.sendKeys('user4@abc.com');
    };

    this.emailFieldFillNegative = function() {
        emailField.sendKeys('user4@');
    };

    this.emailFieldValueCheck = function() {
        expect(emailField.getAttribute('value')).toEqual('user4@abc.com');
    };

    this.addressFieldExist = function() {
        expect(addressField.isPresent()).toBeTruthy();
    };

    this.addressFieldFill = function(){
        addressField.sendKeys('Address4');
    };

    this.addressFieldValueCheck = function() {
        expect(addressField.getAttribute('value')).toEqual('Address4');
    };

    this.resetFormBtnClick = function() {
        resetFormBtn.click();
    };

    this.addUpdateBtnClick = function() {
        addUpdateBtn.click();
    };

    this.updateBtnClick = function() {
        updateBtn.click();
    };

    this.emptyFieldsCheck = function() {
        expect(nameField.getAttribute('value')).toEqual('');
        expect(addressField.getAttribute('value')).toEqual('');
        expect(emailField.getAttribute('value')).toEqual('');
    };

    this.nameFieldCorrectness = function() {
        expect(nameField.getAttribute('class')).toContain('ng-invalid');
    };

    this.emailFieldCorrectness = function () {
        expect(emailField.getAttribute('class')).toContain('ng-invalid');
    };

    this.addUser4 = function() {
        nameField.sendKeys('User4');
        addressField.sendKeys('Address4');
        emailField.sendKeys('user4@abc.com');
        addUpdateBtn.click();
    };

    this.addUser5 = function() {
        nameField.sendKeys('User5');
        addressField.sendKeys('Address5');
        emailField.sendKeys('user5@abc.com');
        addUpdateBtn.click();
    };

    this.addUser6 = function() {
        nameField.sendKeys('User6');
        addressField.sendKeys('Address6');
        emailField.sendKeys('user6@abc.com');
        addUpdateBtn.click();
    };

    this.addUser7 = function() {
        nameField.sendKeys('User7');
        addressField.sendKeys('Address7');
        emailField.sendKeys('user7@abc.com');
        addUpdateBtn.click();
    };

    this.addUser8 = function() {
        nameField.sendKeys('User8');
        addressField.sendKeys('Address8');
        emailField.sendKeys('user8@abc.com');
        addUpdateBtn.click();
    };

    this.addUser9 = function() {
        nameField.sendKeys('User9');
        addressField.sendKeys('Address9');
        emailField.sendKeys('user9@abc.com');
        addUpdateBtn.click();
    };

    this.addUser10 = function() {
        nameField.sendKeys('User10');
        addressField.sendKeys('Address10');
        emailField.sendKeys('user10@abc.com');
        addUpdateBtn.click();
    };

    this.clearFields = function() {
        nameField.clear();
        addressField.clear();
        emailField.clear();
    };

};
module.exports = new UserRegistrationForm_page();