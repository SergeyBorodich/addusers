var ListOfUsers_page = function() {

    var users = element.all(by.repeater('u in controller.users'));
    var editBtn = element(by.repeater('u in controller.users').row(3)).element(by.css('#edit'));
    var removeBtn = element(by.repeater('u in controller.users').row(3)).element(by.css('#remove'));

    this.findLastUser = function() {
        users.last(function(row) {
            var rowElems = row.$$('td');
            expect(rowElems.get(0).getText()).toMatch('User4');
            expect(rowElems.get(1).getText()).toMatch('Address4');
            expect(rowElems.get(2).getText()).toMatch('user4@abc.com');
        });
    };

    this.editBtnClick = function() {
        editBtn.click();
    };

    this.removeBtnClick = function() {
        removeBtn.click();
    };

    this.findLastUser_upd = function() {
        users.last(function(row) {
            var rowElems = row.$$('td');
            expect(rowElems.get(0).getText()).toMatch('User5');
        });
    };

    this.checkDeletedRow = function() {
        expect(users.count()).toBe(3);
    };

    this.checkAdded1User = function() {
        expect(users.count()).toBe(4);
    };

    this.checkAdded7Users = function() {
        expect(users.count()).toBe(10);
    };

};
module.exports = new ListOfUsers_page();