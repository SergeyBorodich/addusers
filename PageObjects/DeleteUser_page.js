var DeleteUser_page = function() {

    var okBtn = element(by.buttonText('OK'));
    var cancelBtn = element(by.buttonText('Cancel'));

    this.okBtnExist = function() {
      expect(okBtn.isPresent()).toBeTruthy();
    };

    this.cancelBtnExist = function() {
        expect(cancelBtn.isPresent()).toBeTruthy();
    };

    this.okBtnClick = function() {
      okBtn.click();
    };

    this.cancelBtnClick = function() {
        cancelBtn.click();
    };
};
module.exports = new DeleteUser_page();