"use strict";

var DeleteUser_page = require('../../PageObjects/DeleteUser_page.js');
var ListOfUsers_page = require('../../PageObjects/ListOfUsers_page.js');

describe('Проверка элементов страницы для удаления пользоваталей', function() {

    it('Перейти на страницу', function() {
        browser.get('http://localhost:8080/TestAppExample/index');
        browser.sleep(1000);
    });

    it('Удалить пользователя №10 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser10();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser10();
    });

    it('Удалить пользователя №9 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser9();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser9();
    });

    it('Удалить пользователя №8 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser8();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser8();
    });

    it('Удалить пользователя №7 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser7();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser7();
    });

    it('Удалить пользователя №6 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser6();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser6();
    });

    it('Удалить пользователя №5 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser5();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser5();
    });

    it('Удалить пользователя №4 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser4();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser4();
    });

    it('Удалить пользователя №3 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser3();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser3();
    });

    it('Удалить пользователя №2 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser2();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser2();
    });

    it('Удалить пользователя №1 из списка и проверить, что он отсутствует в List of Users', function() {
        ListOfUsers_page.removeUser1();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.checkDeletedUser1();
    });

    it('Проверить, что список пользователей пуст', function() {
        ListOfUsers_page.checkEmptyList();
    });

});
