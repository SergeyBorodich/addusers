"use strict";

var UserRegistrationForm_page = require('../../PageObjects/UserRegistrationForm_page.js');
var ListOfUsers_page = require('../../PageObjects/ListOfUsers_page.js');

describe('Проверка элементов страницы для добавления новых пользователей', function() {

    it('Перейти на страницу добавления новых пользователей', function () {
        browser.get('http://localhost:8080/TestAppExample/index');
        browser.sleep(2000);
    });

    it('Ввести в поле Name значение (менее 3-х символов), проверить, что это значение некорректно для этого поля', function() {
        UserRegistrationForm_page.nameFieldNegativeFill();
        browser.sleep(1000);
        UserRegistrationForm_page.nameFieldCorrectness();
    });

    it('Ввести в поле Email значение неправильного формата, проверить, что это значение некорректно для этого поля и кнопка Add неактивна', function() {
        UserRegistrationForm_page.emailFieldFillNegative();
        browser.sleep(1000);
        UserRegistrationForm_page.emailFieldCorrectness();
        UserRegistrationForm_page.addUpdateBtnDisabled();
        UserRegistrationForm_page.clearFields();
    });

    it('Добавить 7 новых пользователей и проверить, что они добавились в List of Users', function () {
        UserRegistrationForm_page.addUser4();
        UserRegistrationForm_page.addUser5();
        UserRegistrationForm_page.addUser6();
        UserRegistrationForm_page.addUser7();
        UserRegistrationForm_page.addUser8();
        UserRegistrationForm_page.addUser9();
        UserRegistrationForm_page.addUser10();
        ListOfUsers_page.checkAdded7Users();
    });
});
