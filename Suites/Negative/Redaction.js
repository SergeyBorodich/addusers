"use strict";

var ListOfUsers_page = require('../../PageObjects/ListOfUsers_page.js');
var UserRegistrationForm_page = require('../../PageObjects/UserRegistrationForm_page.js');

describe('Проверка элементов страницы для редактирования пользоваталей', function() {

    it('Перейти на страницу', function() {
        browser.get('http://localhost:8080/TestAppExample/index');
        browser.sleep(1000);
    });

    it('Найти пользователя №4, нажать Edit, ввести некорректные значения в поля Name и Email и проверить, что кнопка Update неактивна', function() {
        ListOfUsers_page.findUser4();
        ListOfUsers_page.editUser4();
        browser.sleep(1000);
        UserRegistrationForm_page.clearFields();
        UserRegistrationForm_page.nameFieldNegativeFill();
        browser.sleep(1000);
        UserRegistrationForm_page.nameFieldCorrectness();
        UserRegistrationForm_page.emailFieldFillNegative();
        browser.sleep(1000);
        UserRegistrationForm_page.emailFieldCorrectness();
        UserRegistrationForm_page.addUpdateBtnDisabled();
    });

    it('Ввести в поле Name, Address и Email новые корректные значения', function() {
        UserRegistrationForm_page.clearFields()
        browser.sleep(1000);
        UserRegistrationForm_page.nameFieldFill_3();
        UserRegistrationForm_page.addressFieldFill_2();
        UserRegistrationForm_page.emailFieldFillPositive_2();
        browser.sleep(1000);
    });

    it('Нажать кнопку Update и проверить, что значения изменились после редактирования', function() {
        UserRegistrationForm_page.addUpdateBtnClick();
        browser.sleep(1000);
        ListOfUsers_page.findUser4_upd();
    });
});
