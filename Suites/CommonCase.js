"use strict";

var UserRegistrationForm_page = require('../PageObjects/UserRegistrationForm_page.js');
var ListOfUsers_page = require('../PageObjects/ListOfUsers_page.js');
var DeleteUser_page = require('../PageObjects/DeleteUser_page.js');

describe('Проверка элементов страницы для добавления новых пользователей', function() {

    it('Перейти на страницу добавления новых пользователей', function () {
        browser.get('http://localhost:8080/TestAppExample/index');
        browser.sleep(2000);
    });

    it('Удалить всех пользователей', function () {
        ListOfUsers_page.removeClick();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.removeClick();
        DeleteUser_page.okBtnClick();
        ListOfUsers_page.removeClick();
        DeleteUser_page.okBtnClick();
    });
});
