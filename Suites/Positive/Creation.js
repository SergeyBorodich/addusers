"use strict";

var UserRegistrationForm_page = require('../../PageObjects/UserRegistrationForm_page.js');
var ListOfUsers_page = require('../../PageObjects/ListOfUsers_page.js');

describe('Проверка элементов страницы для добавления новых пользователей', function() {

    it('Перейти на страницу добавления новых пользователей', function() {
        browser.get('http://localhost:8080/TestAppExample/index');
        browser.sleep(2000);
    });

    it('Проверить наличие поля Name в User Registration Form и убедиться, что кнопки Add и Reset Form неактивны', function(){
        UserRegistrationForm_page.nameFieldExist();
        UserRegistrationForm_page.addUpdateBtnDisabled();
        UserRegistrationForm_page.resetFormBtnDisabled();
        browser.sleep(1000);
    });

     it('Ввести значение(более 2-х символов) в поле Name, проверить введенное значение и убедиться, что кнопка Add неактивна, а кнопка Reset Form активна', function(){
        UserRegistrationForm_page.nameFieldFill();
        UserRegistrationForm_page.nameFieldValueCheck();
        UserRegistrationForm_page.addUpdateBtnDisabled();
        UserRegistrationForm_page.resetFormBtnEnabled();
    });

      it('Проверить наличие поля Email и ввести значение в формате почтового адреса, проверить введенное значение и убедиться, что кнопки Add и Reset Form активны', function(){
        UserRegistrationForm_page.emailFieldExist();
        browser.sleep(1000);
        UserRegistrationForm_page.emailFieldFillPositive();
        UserRegistrationForm_page.emailFieldValueCheck();
        UserRegistrationForm_page.addUpdateBtnEnabled();
        UserRegistrationForm_page.resetFormBtnEnabled();
    });

    it('Проверить наличие поля Address и ввести значение, проверить введенное значение и убедиться, что кнопки Add и Reset Form активны', function(){
        UserRegistrationForm_page.addressFieldExist();
        browser.sleep(1000);
        UserRegistrationForm_page.addressFieldFill();
        UserRegistrationForm_page.addressFieldValueCheck();
        UserRegistrationForm_page.addUpdateBtnEnabled();
        UserRegistrationForm_page.resetFormBtnEnabled();
        browser.sleep(1000);
    });

    it('Нажать на кнопку Reset Form, проверить, что поля Name, Address и Email являются пустыми и убедиться, что кнопки Add и Reset Form неактивны', function(){
        UserRegistrationForm_page.resetFormBtnClick();
        browser.sleep(1000);
        UserRegistrationForm_page.emptyFieldsCheck();
        UserRegistrationForm_page.addUpdateBtnDisabled();
        UserRegistrationForm_page.resetFormBtnDisabled();
    });

    it('Заполнить поля Name, Address, Email и нажать кнопку Add и проверить, что поля Name, Address и Email являются пустыми и убедиться, что кнопки Add и Reset Form неактивны', function(){
        UserRegistrationForm_page.nameFieldFill();
        UserRegistrationForm_page.addressFieldFill();
        UserRegistrationForm_page.emailFieldFillPositive();
        browser.sleep(1000);
        UserRegistrationForm_page.addUpdateBtnClick();
        browser.sleep(1000);
        UserRegistrationForm_page.emptyFieldsCheck();
        UserRegistrationForm_page.addUpdateBtnDisabled();
        UserRegistrationForm_page.resetFormBtnDisabled();
    });

    it('Проверить, что новый пользователь добавлен в List of Users', function () {
        ListOfUsers_page.checkAdded1User();
        ListOfUsers_page.findLastUser();
    });

});
