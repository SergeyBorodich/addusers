"use strict";

var DeleteUser_page = require('../../PageObjects/DeleteUser_page.js');
var ListOfUsers_page = require('../../PageObjects/ListOfUsers_page.js');

describe('Проверка элементов страницы для удаления пользоваталей', function() {

    it('Перейти на страницу', function () {
        browser.get('http://localhost:8080/TestAppExample/index');
        browser.sleep(2000);
    });

    it('Определить последнего добавленного пользователя и нажать на кнопку Remove', function() {
        ListOfUsers_page.findLastUser_upd();
        ListOfUsers_page.removeLastUser();
        browser.sleep(1000);
    });

    it('Проверить наличие кнопок OK и Cancel, нажать Cancel и убедиться, что пользователь не был удален', function() {
        DeleteUser_page.okBtnExist();
        DeleteUser_page.cancelBtnExist();
        DeleteUser_page.cancelBtnClick();
        ListOfUsers_page.findLastUser_upd();
    });

    it('Нажать на кнопку Remove для последнего пользователя, нажать на кнопку OK и убедиться, что пользователь был удален', function() {
        ListOfUsers_page.removeLastUser();
        browser.sleep(1000);
        DeleteUser_page.okBtnClick();
        browser.sleep(1000);
        ListOfUsers_page.checkDeletedLastRow();
    });

});