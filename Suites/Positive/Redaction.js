"use strict";

var ListOfUsers_page = require('../../PageObjects/ListOfUsers_page.js');
var UserRegistrationForm_page = require('../../PageObjects/UserRegistrationForm_page.js');

describe('Проверка элементов страницы для редактирования пользоваталей', function() {

    it('Перейти на страницу', function() {
        browser.get('http://localhost:8080/TestAppExample/index');
        browser.sleep(2000);
    });

    it('Определить последнего добавленного пользователя', function() {
        ListOfUsers_page.findLastUser();
    });

    it('Нажать на кнопку Edit и проверить, что поля Name, Address и Email заполнены в User Registration Form', function() {
        ListOfUsers_page.editUser4();
        browser.sleep(1000);
        UserRegistrationForm_page.nameFieldValueCheck();
        UserRegistrationForm_page.addressFieldValueCheck();
        UserRegistrationForm_page.emailFieldValueCheck();
    });

    it('Изменить значение в поле Name, нажать кнопку Reset Form и проверить, что поля Name, Address и Email являются пустыми и значения в List of Users не изменилось для последнего пользователя',function() {
        UserRegistrationForm_page.nameFieldFill_2();
        UserRegistrationForm_page.resetFormBtnClick();
        UserRegistrationForm_page.emptyFieldsCheck();
        ListOfUsers_page.findLastUser();
    });

    it('Нажать на кнопку Edit, изменить значение в поле Name и нажать кнопку Update, проверить, что значение поля Name в List of Users изменилось', function() {
        ListOfUsers_page.editUser4();
        browser.sleep(1000);
        UserRegistrationForm_page.nameFieldFill_2();
        browser.sleep(1000);
        UserRegistrationForm_page.addUpdateBtnClick();
        ListOfUsers_page.findLastUser_upd();
    });
});
